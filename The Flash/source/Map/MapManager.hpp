#pragma once

#include "Map.hpp"

#include <unordered_map>
#include <string>

class MapManager
{
public:
	MapManager() { currentMap = nullptr; };

	bool AddMap( const std::string& name, Map* map );
	Map* GetCurrentMap() { return this->currentMap; }
	Map* GetMap( const std::string& name );
	bool LoadMap( const std::string& name );
	void Draw( sf::RenderTarget& target, sf::RenderStates states );
private:
	std::unordered_map<std::string, Map*> maps;
	Map* currentMap;
};