#include "Map.hpp"

#include <iostream>

std::vector<Cell*> Map::GetTiles()
{
	std::vector<Cell*> tilesPtr;
	tilesPtr.reserve( tiles.size() );
	for ( auto& tile : this->tiles ) {
		tilesPtr.push_back( &tile );
	}

	return tilesPtr;
}

sf::Texture* Map::getTileSet()
{
	return &this->tileSet;
}

std::uint16_t Map::getVersion()
{
	return this->version;
}

bool Map::LoadFromFile( const std::string& path )
{
	std::ifstream file( path );
	if ( !file.is_open() )
		return false;

	file >> this->version;

	std::string tileSetPath;
	file >> tileSetPath;
	if ( !this->tileSet.loadFromFile( tileSetPath ) ) {
		return false;
	}

	std::uint16_t width, height, colpos;
	file >> width;
	file >> height;
	file >> colpos;
	if ( width < 0 && height < 0 || colpos < 0 ) {
		return false;
	}

	this->size = sf::Vector2u( width, height );
	this->collPosition = colpos;

	this->tiles.clear();
	this->tiles.reserve( width * height );

	std::uint16_t id;
	for ( auto i = 0; i < height; ++i ) {
		for ( auto j = 0; j < width; ++j ) {
			file >> id;
			if ( id < this->collPosition )
				this->tiles.push_back( Cell( id, sf::Vector2f( tileSize.x * j, tileSize.y * i ), false ) );
			else {
				Cell cell( id, sf::Vector2f( tileSize.x * j, tileSize.y * i ), true );
				this->tiles.push_back( cell );
				this->tilesCollision.push_back( cell );
			}
		}
	}
	return true;
}

void Map::UpdateTilesVerticies()
{
	std::uint16_t width = this->size.x;
	std::uint16_t height = this->size.y;
	this->vertices.setPrimitiveType( sf::Quads );
	this->vertices.resize( width * height * 4 );

	for ( const auto& cell : this->tiles ) {
		std::uint16_t posX = cell.position.x;
		std::uint16_t posY = cell.position.y;
		auto id = cell.id;
		
		std::uint16_t i = posX / tileSize.x, j = posY / tileSize.y;
		
		sf::Vertex* quad = &this->vertices[( i + j * width ) * 4];

		quad[0].position = sf::Vector2f( ( i + 1 ) * tileSize.x, j * tileSize.y );
		quad[1].position = sf::Vector2f( i * tileSize.x, j * tileSize.y );
		quad[2].position = sf::Vector2f( i * tileSize.x, ( j+ 1 ) * tileSize.y );
		quad[3].position = sf::Vector2f( ( i + 1 ) * tileSize.x, ( j + 1 ) * tileSize.y );

		quad[0].texCoords = sf::Vector2f( id * tileSize.x, 0 );
		quad[1].texCoords = sf::Vector2f( ( id + 1 ) * tileSize.x, 0 );
		quad[2].texCoords = sf::Vector2f( ( id + 1 ) * tileSize.x, tileSize.y );
		quad[3].texCoords = sf::Vector2f( id * tileSize.x, tileSize.y );
	}
}

void Map::Draw( sf::RenderTarget& target, sf::RenderStates states )
{
	states.texture = &tileSet;
	target.draw( vertices, states );
}

bool Map::IsCollision(Player* player)
{
	for ( auto tile : this->tilesCollision ) {
		if ( !tile.IsCollider() ) continue;
		if ( sf::FloatRect( player->GetSprite().getGlobalBounds() ).intersects( sf::FloatRect( tile.position, sf::Vector2f( this->tileSize.x, this->tileSize.y ) ) ) ) {
			return true;
		}
	}
	return false;
}

Cell* Map::GetCell( std::int16_t x, std::int16_t y )
{
	for ( auto& tile : this->tiles )
		if ( tile.position == sf::Vector2f( x, y ) )
			return &tile;
}