#include "MapManager.hpp"

#include <iostream>

bool MapManager::AddMap( const std::string& name, Map* map )
{
	auto result = this->maps.find( name );

	if ( result != this->maps.end() ) return false;

	this->maps[name] = map;
	return true;
}

Map* MapManager::GetMap( const std::string& name )
{
	auto result = this->maps.find( name );
	if ( result != this->maps.end() ) {
		return result->second;
	}

	return nullptr;
}

bool MapManager::LoadMap( const std::string& name )
{
	auto result = this->maps.find( name );
	if ( result != this->maps.end() ) {
		this->currentMap = result->second;
		result->second->UpdateTilesVerticies();

		return true;
	}
	return false;
}

void MapManager::Draw( sf::RenderTarget& target, sf::RenderStates states )
{
	this->currentMap->Draw( target, states );
}