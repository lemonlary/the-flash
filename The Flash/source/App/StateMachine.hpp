#pragma once

#include <unordered_map>
#include <memory>
#include <functional>

#include "State.hpp"

class StateMachine
{
	friend class App;
public:
	StateMachine( short start );

	void Run();
	
	template<class T, typename = std::enable_if<std::is_base_of<State, T>::value>>
	void AddState( short id )
	{
		auto result = states.find( id );
		if ( result == states.end() )
			states[id].reset( new T() );
	}

private:
	std::unordered_map<short, std::unique_ptr<State>> states;
	short currentState;

};