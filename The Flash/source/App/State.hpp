#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

class State
{
public:
	State();
	virtual ~State() = default;

	virtual short Run() = 0;

	void SetWindow( sf::RenderWindow* win );
	void SetView( sf::View* view );

protected:
	sf::RenderWindow* window;
	sf::View* view;
};
