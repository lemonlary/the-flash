#pragma once

#include <SFML/Graphics/RenderWindow.hpp>

#include "../Config.hpp"
#include "StateMachine.hpp"

class App
{
public:
	App();
	~App();

	void Run();

	template<class T, typename = std::enable_if<std::is_base_of<State, T>::value>>
	void AddState( short id )
	{
		this->stateMachine.AddState<T>( id );
		this->stateMachine.states[id]->SetWindow( &window );
		this->stateMachine.states[id]->SetView( &view );
	}

	inline sf::RenderWindow& GetWindow() { return this->window; }
private:
	sf::RenderWindow window;
	sf::View view;

	StateMachine stateMachine;
};