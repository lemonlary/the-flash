#include "State.hpp"

State::State()
{
	this->window = nullptr;
}

void State::SetWindow( sf::RenderWindow* win )
{
	this->window = win;
}

void State::SetView( sf::View* view )
{
	this->view = view;
}