#include "StateMachine.hpp"

StateMachine::StateMachine( short id )
{
	this->currentState = id;
}

void StateMachine::Run()
{
	if ( this->states.size() == 0 ) return;

	short next = this->currentState;

	while ( next != -1 ) {
		next = this->states[this->currentState]->Run();

		if ( this->currentState != next ) {
			auto result = this->states.find( next );
			if ( result != this->states.end() ) this->currentState = next;
			else this->currentState = -1;
		}
	}
}