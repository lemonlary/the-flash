#pragma once

#include <string>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>

namespace cfg
{
	namespace app
	{
		constexpr auto title = "The Flash!";
		const sf::Vector2u window = { 800u, 600u };
	}

	namespace player
	{
		namespace controls
		{
			constexpr sf::Keyboard::Key left = sf::Keyboard::A;
			constexpr sf::Keyboard::Key right = sf::Keyboard::D;
			constexpr sf::Keyboard::Key down = sf::Keyboard::S;
			constexpr sf::Keyboard::Key up = sf::Keyboard::W;
			constexpr sf::Keyboard::Key sprint = sf::Keyboard::LShift;
		}
	}
}