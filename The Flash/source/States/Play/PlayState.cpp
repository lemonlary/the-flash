#include "PlayState.hpp"

#include "Resource/ResourceHolder.hpp"

PlayState::PlayState()
{
	ResourceHolder::Load<sf::Texture>( "flash_anim", "data\\animations\\flash.png" );

}

short PlayState::Run()
{
	player.Create( *ResourceHolder::Get<sf::Texture>( "flash_anim" ));

	Map test( "data/maps/test/test.txt" );

	this->mapManager.AddMap( "test", &test );
	this->mapManager.LoadMap( "test" );

	this->view->reset( sf::FloatRect( 0, 0, cfg::app::window.x, cfg::app::window.y ) );
	this->view->setCenter( this->player.GetPosition() );

	sf::Clock clock;
	float deltaTime = 0.0f;

	sf::Event ev;
	while ( window->isOpen() )
	{
		deltaTime = clock.restart().asSeconds();
		while ( window->pollEvent( ev ) )
		{
			if ( ev.type == sf::Event::Closed )
				return (short)-1;
			else if ( ev.type == sf::Event::KeyPressed  && sf::Keyboard::isKeyPressed( sf::Keyboard::Escape ) ) {
				return (short)0;
			}
		}
		
		//Move
		float speed = this->player.GetSpeed();
		float sprint = this->player.GetSprint();

		if ( sf::Keyboard::isKeyPressed( cfg::player::controls::sprint ) ) {
			if ( sprint < 5 ) 
				this->player.SetSprint( sprint + 0.05f );
			speed += sprint;
		} else 
			this->player.SetSprint( 0 );

		sf::Vector2f move( 0.f, 0.f );

		this->player.SetDirection(MOVE_AXIS::Y, MOVE_DIR::NONE);
		this->player.SetDirection(MOVE_AXIS::X, MOVE_DIR::NONE);
		if ( sf::Keyboard::isKeyPressed( cfg::player::controls::left ) ) {
			move.x -= speed;
			this->player.SetDirection( MOVE_AXIS::X, MOVE_DIR::LEFT );
		} else if ( sf::Keyboard::isKeyPressed( cfg::player::controls::right ) ) {
			move.x += speed;
			this->player.SetDirection( MOVE_AXIS::X, MOVE_DIR::RIGHT );
		}
		if ( sf::Keyboard::isKeyPressed( cfg::player::controls::up ) ) {
			move.y -= speed;
			this->player.SetDirection( MOVE_AXIS::Y, MOVE_DIR::UP );
		} else if ( sf::Keyboard::isKeyPressed( cfg::player::controls::down ) ) {
			move.y += speed;
			this->player.SetDirection( MOVE_AXIS::Y, MOVE_DIR::DOWN );
		}

		this->player.Move( move );
		this->view->move( move );

		move = { 0.f, 0.f };

		//Collision
		if ( mapManager.GetCurrentMap()->IsCollision( &this->player ) ) {
			if ( this->player.GetDirection(MOVE_AXIS::X) == MOVE_DIR::LEFT) {
				move.x += speed;
			}
			if ( this->player.GetDirection(MOVE_AXIS::X) == MOVE_DIR::RIGHT) {
				move.x -= speed;
			}
			if ( this->player.GetDirection(MOVE_AXIS::Y) == MOVE_DIR::DOWN) {
				move.y -= speed;
			}
			if ( this->player.GetDirection(MOVE_AXIS::Y) == MOVE_DIR::UP) {
				move.y += speed;
			}
		}

		this->player.Move( move );
		this->view->move( move );

		this->player.Update( deltaTime );

		window->setView( *this->view );
		window->clear(sf::Color::White);

		this->mapManager.Draw( *window, sf::RenderStates::Default );
		this->player.Draw( *window );

		window->display();
	}
	return (short)-1;
}