#pragma once

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Rect.hpp>

class Animation
{
public:
	Animation() = default;
	Animation(sf::Texture* texture, const sf::Vector2u& imgCount, float switchTime);
	~Animation();

	void Update(int row, float time);

	sf::IntRect uvRect;
private:
	sf::Vector2u imgCount;
	sf::Vector2u currImg;

	float totalTime;
	float switchTime;

};